import os
import unittest

import his


class TestIDF(unittest.TestCase):
    def setUp(self):
        self.his = "test.his"
        self.his_out = "test-out.his"

    def tearDown(self):
        try:
            os.remove(self.his_out)
        except FileNotFoundError:
            pass

    def test_his(self):
        ds = his.read(self.his)
        his.write(self.his_out, ds)


if __name__ == "__main__":
    unittest.main()


# Example workflow going from a pandas.DataFrame to an xarray.Dataset
# that will be accepted by his.write
# This example has a single location, which needs to be added as a dimensions.
df = pd.DataFrame(
    {
        "time": time,
        "par_a": data,
        "par_b": data,
    }
)
ds = (
    df.set_index("time")
    .to_xarray()
    .expand_dims("station", axis=1)
    .assign_coords({"station": ["location_a"]})
)
ds.attrs["header"] = "some description"
ds.attrs["t0"] = datetime(2000, 1, 1)
ds.attrs["scu"] = 1
